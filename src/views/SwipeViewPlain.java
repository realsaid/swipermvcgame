package views;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;

import models.SwipeBounce;
import models.RunnableViewableSwipe;
import services.Swipe;
import services.SwipeView;

/**
 * 
 * <b> Implements </b> {@link SwipeView}<br>
 * 
 * The view is a simple rectangle 
 * 
 * @version 1.0.0
 * @author Saheed LASISI
 */
public class SwipeViewPlain implements SwipeView{
	
	/**
	 * Local canvas class for inside of {@link SwipeViewPlain}
	 * frame responsible for graphical representation of the {@link SwipeBounce} model
	 * @version 1.0.0
	 * 
	 * @author Saheed LASISI
	 *
	 */
	 class SwipeViewCanvas extends Canvas{

		private final Color BACKGROUND_COLOUR = Color.blue;
		private final Color SWIPE_COLOR = Color.yellow;
		private final int SWIPE_HEIGHT = 8;
		private final int SWIPE_WIDTH;
		private final int CANVAS_WIDTH;
		private final int CANVAS_HEIGHT;
		private final int SWIPE_INCREMENT;
		
		
		private static final long serialVersionUID = 1L;
		
		private RunnableViewableSwipe sweepModel;
		
		public SwipeViewCanvas (RunnableViewableSwipe rvSweep, int width, int height){
			
			super();
			sweepModel = rvSweep;
			CANVAS_WIDTH = width;
			CANVAS_HEIGHT = height;
			setSize(CANVAS_WIDTH, CANVAS_HEIGHT);
			setBackground(BACKGROUND_COLOUR);
			SWIPE_INCREMENT = CANVAS_WIDTH/(Swipe.MAXIMUM_position - Swipe.MINIMUM_position);
			SWIPE_WIDTH= SWIPE_INCREMENT;
		}
		


		public void update(Graphics g){paint(g);}
		
		public void paint(Graphics g){
			
			g.setColor(BACKGROUND_COLOUR);
			g.fillRect(0,
					    CANVAS_HEIGHT-2*SWIPE_HEIGHT-10,
					    CANVAS_WIDTH,
					    SWIPE_HEIGHT);
			
			g.setColor(SWIPE_COLOR);
			g.fillRect(sweepModel.get_position()*SWIPE_INCREMENT,CANVAS_HEIGHT-2*SWIPE_HEIGHT-10,SWIPE_WIDTH,SWIPE_HEIGHT); 
	 	}
	 }
	 
	/**
	 * The frame which contains the canvas in which the {@link Swipe} view is to be painted and which generates the
	 * events that need to be handled by the {@link controllers.SwipeControllerAbstraction}
	 */
	private  JFrame frame;
	
	/**
	 * The canvas in which the view is painted
	 */
	private SwipeViewCanvas canvas;
	
	/**
	 * @param rvSweep is the {@link SwipeBounce} model which is to be associated with the view
	 */
	public SwipeViewPlain(RunnableViewableSwipe rvSweep){
		
		frame = new JFrame("Sweep MVC");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(VIEW_WIDTH,VIEW_HEIGHT+20);
		frame.setVisible(true);
		frame.setResizable(false);
		canvas = new SwipeViewCanvas(rvSweep, VIEW_WIDTH, VIEW_HEIGHT);
		
		frame.getContentPane().add(canvas, BorderLayout.CENTER );
	}
	
	public JFrame getFrame(){return frame;}
	
	public void updateView(){canvas.repaint();}
	
}



