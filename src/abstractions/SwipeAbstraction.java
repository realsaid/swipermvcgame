package abstractions;

import java.util.Random;

import services.Swipe;
import tools.HasInvariant;

/**
 * <b> Implements</b>  {@link HasInvariant}, {@link Swipe}<br><br>
 * 
 * A simple Swipe model for use in a video game.<br>
 *  * It abstracts away from the movement of a swipe as specified in {@link Swipe#updatePosition}<br>
 * 
 * @version 1.0.0
 * @author Saheed LASISI
 */
public abstract class SwipeAbstraction implements HasInvariant, Swipe{
	
	/**
	 * The swipe has one degree of freedom - either left or right. 
	 */
	protected boolean directionToRight;
	
	/**
	 * The horizontal position of the Swipe
	 */
	protected int position;
	
	/**
	 * The position must be within the defined limit: 
	 * {@link Swipe#MINIMUM_position} ... {@link Swipe#MAXIMUM_position} </code>
	 */
	public boolean invariant (){
		
		return (position>= MINIMUM_position && position <= MAXIMUM_position);
	}
	
	public int get_position(){ return position;}
	

	public boolean goingRight(){ return directionToRight;}
	
	/**
	 * Tested by  {@link test.resources.JUnit_Swipe#testUpdatePosition()}, which guarantees that the Swipe remains 
	 * in a safe state as specified by {@link Swipe#invariant}.<br>
	 * The concrete subclasses need to implement this method
	 */
	public abstract void updatePosition();
	
	/**
	 * Tested by  {@link test.resources.JUnit_Swipe#testChangeDirection()}, which guarantees that the Swipe 
     * remains in a safe state as specified by {@link Swipe#invariant}.
	 */
	public void changeDirection(){directionToRight= ! directionToRight; }
	
	/**
	 * Tested by  {@link test.resources.JUnit_Swipe#testDefaultConstructor()}, which guarantees that the Swipe is
     * constructed in a safe state as specified by {@link Swipe#invariant}.<br><br>
     * 
     * As default a Swipe is at the leftmost position as defined by {@link MINIMUM_position}, and moving right
	 */
	public SwipeAbstraction (){
		
		position = MINIMUM_position;
		directionToRight = true;
	}
	
	/**
	 * Tested by  {@link test.resources.JUnit_Swipe#testNonDefaultConstructor()}, which guarantees that the Swipe is
     * constructed in a safe state as specified by {@link Swipe#invariant}.<br><br>
     * 
     * @param pos specifies the initial horizontal position for the Swipe being constructed
     * <ul>
     * <li> if <b> pos </b> is smaller than minimum bound then the position is initialised to the minimum</li>
     * <li> if <b> pos </b> is bigger than maximum bound then the position is initialised to the maximum</li>
     * <li> the Swipe direction is initially set to the right irrespective of the position</li>
     * </ul>
	 */
	public SwipeAbstraction (int pos){
		
		if (pos < MINIMUM_position) position = MINIMUM_position;
		else if (pos > MAXIMUM_position) position = MAXIMUM_position;
		else position = pos;
		directionToRight = true;
	}
	
	/**
	 * Tested by  {@link test.resources.RandomTest_Swipe}, which guarantees that the Swipe is
     * constructed in a safe state as specified by {@link Swipe#invariant}.<br><br>
     * 
     * @param rng is the random number generator to be used in the construction of a random Swipe:
     * <ul>
     * <li> Every valid position has an equal chance of being the initial value </li>
     * <li> Each direction (left and right) has an equal chance of being the initial value </li>
     * </ul>
	 */
	public SwipeAbstraction (Random rng){
		
		position = rng.nextInt(MAXIMUM_position);
		directionToRight = rng.nextBoolean();
	}

	
	public boolean equals( Object thing){
		
		if (thing ==null) return false;
		if ( this == thing) return true;
		if (! (thing instanceof Swipe)) return false;
		
		SwipeAbstraction that = (SwipeAbstraction) thing;
		return ( (this.get_position() == that.get_position()) 
				&& (this.directionToRight == that.directionToRight) );
	}
	

	public String toString(){
		
	String str = "Swipe: position = "+position+", moving =  ";
	if (directionToRight) str = str+"right,";
	else str = str+"left,";
	if (invariant()) str = str+" is in safe state.";
	else str = str+" is not in safe state.";
	return str;
	}

}