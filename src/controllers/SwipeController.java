package controllers;

import java.awt.event.KeyEvent;

import models.RunnableViewableSwipe;
import abstractions.SwipeControllerAbstraction;

/**
 * <b> Extends </b> {@link SwipeControllerAbstraction}<br><br>
 * A simple Swipe controller for use in a video game.<br>
 * Typing a character on the keyboard changes direction of movement of the Swipe. <br>
 * 
 * 
 * @version 1.0.0
 * @author Saheed LASISI
 */
public class SwipeController extends SwipeControllerAbstraction{
	
	/**
	 * The model being controlled by the controller
	 */
	RunnableViewableSwipe Swipe;
	
	/**
	 * @param rvSwipe is the model to be controlled by the controller
	 */
	public SwipeController(RunnableViewableSwipe rvSwipe){
		
		this.Swipe = rvSwipe;
	}

   /**
    * Change direction when a key is typed
    */
   public void keyTyped(KeyEvent e){
	   
	  Swipe.changeDirection();
    }
   
}
