package models;


import java.util.Random;

import abstractions.SwipeAbstraction;
import services.Swipe;
import tools.HasInvariant;

/**
 * <b> Implements</b>  {@link HasInvariant}, {@link Swipe}<br><br>
 * 
 * A simple bouncing Swipe model for use in a video game.<br>
 * 
 * @version 1.0.0
 * @author Saheed LASISI
 */
public class SwipeBounce extends SwipeAbstraction{
	
	
	public SwipeBounce(int i) {
		super(i);
	}

	public SwipeBounce() {
		super();
	}

	public SwipeBounce(Random rng) {
		super(rng);
	}

	/**
	 * Tested by  {@link test.resources.JUnit_Swipe#testUpdatePosition()}, which guarantees that the Swipe remains 
	 * in a safe state as specified by {@link SwipeBounce#invariant}. <br>
	 * 
	 * When the Swipe reaches the end of the screen we chose to make it bounce back by changing direction
	 */
	public void updatePosition(){
		
	 if (directionToRight && position < MAXIMUM_position) position++;
	 else if (!directionToRight && position > MINIMUM_position) position--;
	 else changeDirection();
	}
	
}
