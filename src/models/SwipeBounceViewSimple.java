package models;

import controllers.SwipeController;
import abstractions.SwipeControllerAbstraction;
import services.SwipeView;
import views.SwipeViewPlain;
import models.RunnableViewableSwipe;

/**
 * A first step in building a java game where a Swipe constantly moves horizontally at the 
 * bottom of a 2-D screen and its direction is changed/controlled by keyboard presses<br>
 * 
 * <ul>
 * <li> Model is {@link RunnableViewableBounceSwipe}</li>
 * <li> View is {@link SwipeViewPlain}</li>
 * <li> Controller is {@link SwipeController}</li>
 * </ul>
 * 
 * <b> NOTE: </b> This is not intended as a good example of UI development in Java, it is intended only as
 *                a good introduction to the MVC design pattern<br>
 * @version 1.0.0
 * @author Saheed LASISI
 */
public class SwipeBounceViewSimple {
	
	/**
	 * The model
	 */
	RunnableViewableSwipe rvSwipe;
	
	/** 
	 * The view
	 */
	SwipeView swipeView;
	
	/**
	 * The controller
	 */
	SwipeControllerAbstraction SwipeController;
	
	public SwipeBounceViewSimple(){
		
		// Construct model
		rvSwipe = new RunnableViewableSwipe(new SwipeBounce());
		
		// Construct view which can see model
		swipeView = new SwipeViewPlain(rvSwipe);
		
		//Allow the model to see view in order to make updates when state changes
		rvSwipe.setView(swipeView);
		
		//Construct controller
		SwipeController SwipeController = new SwipeController(rvSwipe);
		
		//The frame which contains the view must allow the controller to react to key presses
		swipeView.getFrame().addKeyListener(SwipeController);
	}
	
	public void startgame(){ 		
		
	  Thread SwipeThread = new Thread((Runnable) rvSwipe);
	  SwipeThread.run();
	  System.out.println("made bounce thread");
	}
	
}
