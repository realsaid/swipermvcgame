package models;


import services.Swipe;
import services.SwipeView;

/**
 * <b> Implements</b>  {@link Runnable}<br>
 * 
 * @version 1.1.0
 * @author Saheed LASISI
 */
public class RunnableViewableSwipe implements Runnable, Swipe{
	/**
	 * The Swipe that will be run
	 */
	private final Swipe swipe;
	
	/**
	 * A 1/10th second delay between movements of Swipe
	 */
	public static final int DELAY = 100;
	
	
	/**
	 * The view to be informed that the state of the Swipe has changed
	 */
	private SwipeView swipeView;
	
	/**
	 * @param p the Swipe to be run
	 */
    public RunnableViewableSwipe(Swipe p){
    	swipe = p;
    }
	
	
	/**
	 * @param SwipeView2 the current view which responds to state changes
	 */
	public void setView(SwipeView SwipeView2){
		swipeView = SwipeView2;
	}
	
	
	public int get_position(){
		return swipe.get_position();
	}
	
	public void changeDirection(){
		swipe.changeDirection();
	}
	
	public boolean goingRight(){
		return swipe.goingRight();
	}
	
	public void updatePosition(){
		swipe.updatePosition();
	}
	
	/**
	 * Every 10th of second:
	 * <ul>
	 * <li> update the Swipe position</li>
	 * <li> inform the view (if it has been initialised) of the update </li>
	 * </ul>
	 */
	public void run(){
		
		do{
	try {
		Thread.sleep(DELAY);
	} catch (InterruptedException e) {e.printStackTrace();}
	
	swipe.updatePosition();
	if (swipeView !=null) swipeView.updateView();
	}
	while (true);
	}
}

