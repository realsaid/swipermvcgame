package services;

import javax.swing.JFrame;

/**
 * Service Specification of a simple Swipe view for use in a video game:
 * <ul>
 * <li> This view is fixed as a 360*360 square</li>
 * </ul>
 * 
 * @version 1.0.0
 * @author Saheed LASISI
 */
public interface SwipeView {
	
	 final int VIEW_WIDTH = 360;
	 final int VIEW_HEIGHT = 360;
	
	/**
	 * @return the frame which contains the canvas in which the view is to be painted and which generates the
	 * events that need to be handled by the controller
	 */
	public JFrame getFrame();
	
	/**
	 * update the canvas on which the view is painted
	 */
	public void updateView();

}
