package applications;


import models.SwipeBounceViewSimple;


/**
 * Instantiates {@link SwipeBounceViewSimple} and starts its execution <br>
 * Entry Point for the SwipeView MVC Application
 * @version 1.0.0
 * @author Saheed LASISI
 * 
 */
public class SwipeMVC_Application {
	
	public static void main(String[] args){
		
		SwipeBounceViewSimple application = new SwipeBounceViewSimple();
		application.startgame();
	
		
	}

}
