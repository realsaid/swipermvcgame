package test.resources;

import models.SwipeBounce;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * <b> Tests</b>  {@link SwipeBounce}<br>
 * 
 * @version 1.0.0
 * @author Saheed LASISI
 */
public class JUnit_Swipe {
	
	protected SwipeBounce SwipeDefault;
	protected SwipeBounce SwipeOK1;
	protected SwipeBounce SwipeOK2;
	protected SwipeBounce SwipePastMax;
	protected SwipeBounce SwipePastMin;
	
	@Before
	public void setUp() throws Exception {
		
			SwipeDefault = new SwipeBounce();
			SwipeOK1 = new SwipeBounce((SwipeBounce.MAXIMUM_position - SwipeBounce.MINIMUM_position)/2);
			SwipeOK2 = new SwipeBounce((SwipeBounce.MAXIMUM_position - SwipeBounce.MINIMUM_position)/2);
			SwipePastMax = new SwipeBounce(SwipeBounce.MAXIMUM_position +1);
			SwipePastMin = new SwipeBounce(SwipeBounce.MINIMUM_position -1);
	}
	
	@After
	public void tearDown() throws Exception {
		
			SwipeDefault = null;
			SwipeOK1 = null;
			SwipePastMax = null;
			SwipePastMin = null;
			
	}
	
	/**
	 * tests the default constructor {@link SwipeBounce#Swipe()}
	 */
	@Test
	public void testDefaultConstructor() {

			Assert.assertTrue(SwipeDefault.invariant());
			Assert.assertEquals(SwipeDefault.get_position(), 0);
			Assert.assertTrue(SwipeDefault.goingRight());	    
	}
	
	/**
	 * tests the default constructor {@link SwipeBounce#Swipe(int)}
	 */
	@Test
	public void testNonDefaultConstructor() {

			Assert.assertTrue(SwipeOK1.invariant());
			Assert.assertEquals(SwipeOK1.get_position(),(SwipeBounce.MAXIMUM_position - SwipeBounce.MINIMUM_position)/2 );
			Assert.assertTrue(SwipeOK1.goingRight());
			
			Assert.assertTrue(SwipePastMax.invariant());
			Assert.assertEquals(SwipePastMax.get_position(),SwipeBounce.MAXIMUM_position);
			Assert.assertTrue(SwipePastMax.goingRight());
			
			Assert.assertTrue(SwipePastMin.invariant());
			Assert.assertEquals(SwipePastMin.get_position(),SwipeBounce.MINIMUM_position);
			Assert.assertTrue(SwipePastMin.goingRight());
	}
	
	/**
	 * tests changeDirection {@link SwipeBounce#changeDirection()}
	 */
	@Test
	public void testChangeDirection() {
		
		boolean direction = SwipeDefault.goingRight();
	
		SwipeDefault.changeDirection();
		Assert.assertTrue(direction != SwipeDefault.goingRight());
		SwipeDefault.changeDirection();
		Assert.assertTrue(direction == SwipeDefault.goingRight());
		
	}
	
	/**
	 * tests updatePosition {@link SwipeBounce#updatePosition()}
	 */
	@Test
	public void testUpdatePosition() {
		
		SwipeDefault.updatePosition();
		Assert.assertTrue(SwipeDefault.invariant());
		Assert.assertEquals(SwipeDefault.get_position(), 1);
		Assert.assertTrue(SwipeDefault.goingRight());
		
		SwipeOK1.updatePosition();
		Assert.assertTrue(SwipeOK1.invariant());
		Assert.assertEquals(SwipeOK1.get_position(),(SwipeBounce.MAXIMUM_position - SwipeBounce.MINIMUM_position)/2 +1);
		Assert.assertTrue(SwipeOK1.goingRight());
		
		SwipePastMax.updatePosition();
		Assert.assertTrue(SwipePastMax.invariant());
		Assert.assertEquals(SwipePastMax.get_position(),SwipeBounce.MAXIMUM_position);
		Assert.assertFalse(SwipePastMax.goingRight());
		
		SwipePastMin.updatePosition();
		Assert.assertTrue(SwipePastMin.invariant());
		Assert.assertEquals(SwipePastMin.get_position(),SwipeBounce.MINIMUM_position+1);
		Assert.assertTrue(SwipePastMin.goingRight());
		
		SwipePastMin.changeDirection();
		SwipePastMin.updatePosition();
		SwipePastMin.updatePosition();
		Assert.assertTrue(SwipePastMin.invariant());
		Assert.assertEquals(SwipePastMin.get_position(),SwipeBounce.MINIMUM_position);
		Assert.assertTrue(SwipePastMin.goingRight());
	}
		
		/**
		 * tests toString {@link SwipeBounce#toString()}
		 */
		@Test
		public void testToString() {
			
			Assert.assertEquals(SwipeDefault.toString(), 
					            "Swipe: position = 0, moving =  right, is in safe state." );
		}

		/**
		 * tests equals {@link SwipeBounce#equals()}
		 */
		@Test
		public void testEquals() {
			
			Assert.assertEquals(SwipeDefault, SwipeDefault);
			Assert.assertEquals(SwipeOK1, SwipeOK2);
			Assert.assertNotSame(SwipeDefault, SwipeOK1);
		}
}
