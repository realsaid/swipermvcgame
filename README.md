Swipe MVC Game
==================================

Scenario
----------
A simple behavior to be integrated with a Video Game to simulate horizontal swipe.
This is to demonstrate knowledge of software design and development using the MVC Pattern.  
The Software design should be maintainable, extensible and easily re-engineered using the MVC Design pattern.



Introduction
-------------
Different Video games comes with the swipe effect. This challenge is aimed at simulating this behavior for a horizontal swipe mechanism that can be consumed by a Video Game application.


# Structure
The packages are organized as :
 - Abstractions : Abstractions for a Swipe and SwipeController Behavioral Abstraction.
 - Applications : The Application entry points of this component.
 - Controllers :  Request Handlers/Intermediaries between the model and the view. User actions and Commands 				    				 invocations and events.
 - Models 	   :  The actual implementation of the application logic
 - Services    : Service API interfaces for a Swipe and a SwipeView which are called as needed in Querying the 					model state, notifying changes in the model state and invoking controller service methods.
 - views	   : The actual graphical representation to the end user.
 - tests	   : Self-verifying specification as test and randomly generated tests 
 - tools	   : Reusable utilities which are not part of the application but proves handy. 
 
# Instructions 
 1. Fork and/or clone this application from the given git repository.
 2. Import as a java project in your IDE
 3. Add JDK 1.8 and JUnit 4 in your buildPath
 4. Review the Code structure for your perusal
 5. Run the application using the main class : applications.SwipeMVC_Application
 
 Or just run the maven command 
 
 ```
 mvn clean package
 ```
 
 ```
  java -jar SwiperMVCGame-0.0.1-SNAPSHOT.jar
  ```
  
 
